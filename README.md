# SymAware :red_car:

## Welcome to Symaware

[SymAware](https://www.symaware.eu/#/) addresses the fundamental need for a new conceptual framework for awareness in multi-agent systems (MASs) that is compatible with the internal models and specifications of robotic agents and that enables safe simultaneous operation of collaborating autonomous agents and humans.

The SymAware framework will use compositional logic, symbolic computations, formal reasoning, and uncertainty quantification to characterise and support situational awareness of MAS in its various dimensions, sustaining awareness by learning in social contexts, quantifying risks based on limited knowledge, and formulating risk-aware negotiation of task distributions

## Code of Conduct
Contributons to the project are allowed upon consent from the project managers. In order to make a contributiion, we highly recommend following the coding advised proposed from the [Google Python Coding standards](https://google.github.io/styleguide/pyguide.html)



## Contact :phone:

- Gregorio Marchesini (gremar@kth.se)
- Arabinda Ghosh (arabinda@mpi-sws.org) 
- Zengjie Zhang (z.zhang3@tue.nl)


## Funding information
This project has received funding from the Horizon Europe- the Framework Programme for Resaerch and Innovation, under that grant agreement 101070802.
