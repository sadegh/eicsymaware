import numpy as np
import matplotlib.pylab as plt

# 1) Greg comment : Maybe we should consider NOT having methods inside this calss (treat it a structure in C++)
# 2) Greg comment : Good if we do not mix upper and lower case Camel noation. Classes start with Uppercase, but attributes should start with lowercase unless they are not classes itself (like CommunicationSocket)
#                   We could also opt to use this guidelines to avoid further confusion (Google standards) https://google.github.io/styleguide/pyguide.html
#                   We NEED to make a decision as soon as possible as to avoid having to rewrite a lot of code. For now this is only an experiment so it is ok.
# 3) Greg Comment : We should be really careful in transmitting the complete Situational Awareness vector. This vector contains a lot of information from each agent and most of it won't be used. If you have many agents, this will create for sure some 
#                   overhead. I suggest that we should decide on what information we would like to transimit (like only the agent state and intent for example). Other agents might not need to know 
#                   all my situationial awareness vector. 
# 4) Greg Comment : the functions : knowledge_awareness, estimation_and_perception,collect_communicated_data need to update the class and should not return anything in my personal opinion. This blcoks should really be
#                   as update blocks that receive information and update the internal state of the agent. We will then ahave a communicattion block that will be used to 
#                   collect and broadcast information. Once we have called these three functions in sequence, all the SA vector should be fully updated and ready for the com[utation of the planning and control
# Arabinda Comment: I changed the structure of the code (Put the agent class at first, added some comments)


STEPPING_TIME = 0.2

# Agent Class

class Agent:
    """Agent class

    Attributes
    ---------
    ID                  (int)           : the id of the agent
    neighbour_list      (list of int)   : the id list of neighbours
    agent_state         (numpy.array)   : the physical state of the agent
    sa                  (class SituationAwareness): the situation awareness object
    transmitted_socket  (class CommunicationSocket) : the transmitted communication socket of this agent
    received_sockets    (list of class CommunicationSocket) : the received communication sockets from the neighbours
    control_input       (numpy.array)   : the control input of the agent
    """

    ID = 0
    neighbour_list = []
    received_sockets = []

    def __init__(self, agent_id, neighbours, initial_agent_state, initial_sa_state, initial_sa_intent, initial_goal):
        self.ID = agent_id
        self.neighbour_list = neighbours
        self.agent_state = initial_agent_state
        # collect messages and update SA
        self.sa = SituationAwareness(initial_sa_state=initial_sa_state, initial_sa_intent=initial_sa_intent)
        self.control_input = self.planning_and_control(self.sa, initial_goal)
        self.transmitted_socket = CommunicationSocket(agent_id=self.ID,
                                                      agent_state=self.agent_state,
                                                      agent_control=self.control_input)

    def perform_simulate(self, agent_goal):
        """perfrom simulation of the agent dynamics over time"""

        self.estimation_and_perception()
        self.knowledge_awareness_update()

        self.sa.update_uncertainty_measure()
        self.sa.update_risk_measure()
        self.control_input = self.planning_and_control(self.sa, agent_goal)
        self.state_step(self.control_input)

    def state_step(self, control_input):

        self.agent_state = self.agent_state + dynamics(self.agent_state, control_input)

    def knowledge_awareness_update(self):
        """updates current knowledge broadcast message and returns updated knowledge"""

        # update current internal knowledge (TODO)

    def estimation_and_perception(self):
        """estimate/ perceive agent state and state of the neighbouring agents"""

        # perform estimation and perception (TODO)

    def planning_and_control(self, situation_awareness, control_goal):
        """computes plan and control input for the current state"""

        control_input = 0.3 * (control_goal - self.agent_state)  # simple P controller to make a showcase
        return control_input

    def transmitted_message(self):
        # update Situational Awareness for transmission
        self.transmitted_socket.agent_situation_awareness = self.sa
        self.transmitted_socket.socket_agent_control = self.control_input
        self.transmitted_socket.socket_agent_state = self.agent_state
        return self.transmitted_socket


## System Dynamics

def dynamics(current_state, control_input):

    # single integrator
    next_state = control_input * STEPPING_TIME

    return next_state

## Situational Awareness

class SituationAwareness:
    """
    The Class For Situational Awareness
  
    Attributes 
    ----------
    sa_state            (int)   : the internal state of situation awareness
    sa_intent           (int)   : the intent variable of situation awareness
    uncertainty_measure (float) : the uncertainty measure
    risk_measure        (float) : the risk measure
    """

    uncertainty_measure = 0
    risk_measure = 0

    def __init__(self, initial_sa_state, initial_sa_intent):
        self.sa_state = initial_sa_state
        self.sa_intent = initial_sa_intent

    def update_uncertainty_measure(self):
        self.uncertainty_measure = 0

    def update_risk_measure(self):
        self.risk_measure = 0

## Perception module

class Perception:

    # This class is to be defined (TO DO)
    pass

## Knowledge Module

class Knowledge:

    # This class is to be defined (TO DO)
    pass

## Communication module

class CommunicationSocket:
    """
    The Class For Communication Sockets
    
    Attributes 
    ----------
    agent_id                (int) : the id of the agent owning this socket
    agent_knowledge         (class Knowledge) : the knowledge transmitted by the agent
    agent_perception        (class Perception) : the perception made by the agent
    agent_situation_awareness(class SituationAwareness) : the situation awareness inferred by the agent
    socket_agent_control           (numpy.array) : the control input of this agent
    socket_agent_state             (numpy.array) : the state of this agent

    """
    socket_agent_id = 0
    agent_knowledge = Knowledge()
    agent_perception = Perception()
    agent_situation_awareness = SituationAwareness(initial_sa_state=0, initial_sa_intent=0)

    def __init__(self, agent_id, agent_state, agent_control):
        self.socket_agent_id = agent_id
        self.socket_agent_state = agent_state
        self.socket_agent_control = agent_control


## For Simulation

number_of_agents = 5
simulation_iterations = 500
# Define the topology of the mas communication network
mas_topology = {0: [1, 2, 3, 4],
                1: [0, 2, 3, 4],
                2: [0, 1, 3, 4],
                3: [0, 1, 2, 4],
                4: [0, 1, 2, 3]}


initial_agent_states = np.random.randint(low=1, high=20, size=number_of_agents)
initial_sa_states = np.zeros([number_of_agents, ])
initial_sa_intents = np.zeros([number_of_agents, ])

agents_goals = np.random.randint(low=1, high=20, size=number_of_agents)
agent_trajectories = [np.empty(shape=(simulation_iterations,)) for kk in range(number_of_agents)]

agents_list = [Agent(agent_id=agent_ID, neighbours=mas_topology[agent_ID],
                     initial_agent_state=initial_agent_states[agent_ID],
                     initial_sa_state=initial_sa_states[agent_ID],
                     initial_sa_intent=initial_sa_intents[agent_ID],
                     initial_goal=agents_goals[agent_ID]) for agent_ID in range(number_of_agents)]


for t in range(simulation_iterations):

    # messages collection phase
    for agent in agents_list:
        # gather messages from other agents
        agent.received_sockets = [agents_list[j].transmitted_message() for j in agent.neighbour_list]

    # dynamics step phase
    for agent_id, agent, goal in zip(range(number_of_agents), agents_list, agents_goals):

        # save state before taking the step
        agent_trajectories[agent_id][t] = agent.agent_state
        # simulate next move
        agent.perform_simulate(goal)
        
    
fig, ax = plt.subplots()
for jj in range(number_of_agents):
    
    plt.scatter([0], [initial_agent_states[jj]], c="green")
    plt.scatter([simulation_iterations-1], [agents_goals[jj]], c="red")
    plt.plot(range(simulation_iterations), agent_trajectories[jj], label="Agent "+str(jj))

ax.set_title("State Trajectories\n Start Green Dot- Arrive Red Dot")
ax.set_xlabel("simulation_iterations")
ax.set_ylabel("agent states")
ax.legend()
plt.show()
